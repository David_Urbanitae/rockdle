import { idText } from "typescript";
import { rawSongs } from "./rawSongs";

// https://developers.google.com/youtube/v3/docs/search/list?apix=true&apix_params=%7B%22part%22%3A%5B%22id%22%2C%22snippet%22%5D%2C%22maxResults%22%3A50%2C%22order%22%3A%22viewCount%22%2C%22q%22%3A%22Baron%20Rojo%22%2C%22regionCode%22%3A%22ES%22%2C%22type%22%3A%5B%22video%22%5D%2C%22videoEmbeddable%22%3A%22true%22%2C%22videoSyndicated%22%3A%22true%22%7D

// const artist = "Barón Rojo";

// const result = rawSongs.items.map(item => {
//   return {
//     artist,
//     name: item.snippet.title,
//     youtubeId: item.id.videoId,
//   }
// })
// .filter(item => item.name.startsWith(artist))
//   .map(item => ({ ...item, name: item.name.replace(artist + " - ", "") }))
//   .map(item => ({ ...item, name: item.name.replace(artist + "- ", "") }))
//   .map(item => {
//     const parenthesis = item.name.indexOf("(");
//     return {
//       ...item,
//       name: parenthesis > 0 ? item.name.substring(0, item.name.indexOf("(")).trim() : item.name.trim()
//     }
//   })
// console.log(result);

export const songs = [
  {
      "artist": "Dover",
      "name": "Devil came to me",
      "youtubeId": "D9U-A3mwTw0"
  },
  {
      "artist": "La Raíz",
      "name": "El Tren Huracán | Videoclip Oficial",
      "youtubeId": "pgozJRbwKfM"
  },
  {
      "artist": "Mägo de Oz",
      "name": "Hasta que el cuerpo aguante",
      "youtubeId": "_fnpRds601s"
  },
  {
      "artist": "La Raíz",
      "name": "Elegiré",
      "youtubeId": "rRJ4dsmmTDI"
  },
  {
      "artist": "Extremoduro",
      "name": "So Payaso",
      "youtubeId": "1Iw1Qx7c2yc"
  },
  {
      "artist": "Barón Rojo",
      "name": "Los Rockeros Van Al Infierno",
      "youtubeId": "9og-k5pBe4Y"
  },
  {
      "artist": "Boikot",
      "name": "Hasta siempre",
      "youtubeId": "HV6itZtsv0I"
  },
  {
      "artist": "Extremoduro",
      "name": "Salir",
      "youtubeId": "Wll8HpTuD7g"
  },
  {
      "artist": "Héroes del Silencio",
      "name": "La chispa adecuada",
      "youtubeId": "jnDjEHyhFpU"
  },
  {
      "artist": "Barón Rojo",
      "name": "Hijos De Cain",
      "youtubeId": "4BxTsqrlBZM"
  },
  {
      "artist": "Reincidentes",
      "name": "Vicio",
      "youtubeId": "ykPMPG13ABM"
  },
  {
      "artist": "Ska-P",
      "name": "La Estampida",
      "youtubeId": "bDeIXD7wRsQ"
  },
  {
      "artist": "Héroes del Silencio",
      "name": "Maldito duende",
      "youtubeId": "SqB4FSettTI"
  },
  {
      "artist": "Mägo de Oz",
      "name": "La danza del fuego",
      "youtubeId": "NbrGRuDIfGw"
  },
  {
      "artist": "Marea",
      "name": "El hijo de la Inés",
      "youtubeId": "-m7qmjUVhLQ"
  },
  {
      "artist": "Boikot",
      "name": "Cualquier dia",
      "youtubeId": "_GVQ8_1P3Fo"
  },
  {
      "artist": "Marea",
      "name": "Manuela canta saetas",
      "youtubeId": "p55v9q0ysMY"
  },
  {
      "artist": "Extremoduro",
      "name": "Puta",
      "youtubeId": "L3zMbvleA1A"
  },
  {
      "artist": "Los Suaves",
      "name": "Palabras para Julia",
      "youtubeId": "P8WvQbkOpyY"
  },
  {
      "artist": "Héroes del Silencio",
      "name": "La sirena varada",
      "youtubeId": "IyCw0DabDsk"
  },
  {
      "artist": "Marea",
      "name": "Corazon de mimbre",
      "youtubeId": "WdZcC0nowBs"
  },
  {
      "artist": "La Raíz",
      "name": "Radio Clandestina",
      "youtubeId": "m1OF3_ZGET4"
  },
  {
      "artist": "Mägo de Oz",
      "name": "La costa del silencio",
      "youtubeId": "JV8-fFpMnsk"
  },
  {
      "artist": "Dover",
      "name": "King George",
      "youtubeId": "OR-viYufiY8"
  },
  {
      "artist": "Marea",
      "name": "Trasegando",
      "youtubeId": "VKMIaSdsuVk"
  },
  {
      "artist": "Marea",
      "name": "Que se joda el viento",
      "youtubeId": "s5jLUuK8D44"
  },
  {
      "artist": "Boikot",
      "name": "Korsakov",
      "youtubeId": "Wk5su0_LMLs"
  },
  {
      "artist": "La Raíz",
      "name": "A la sombra de la sierra",
      "youtubeId": "f1BQsyitRDE"
  },
  {
      "artist": "La Raíz",
      "name": "Entre Poetas y Presos",
      "youtubeId": "Zp40yUFNXqs"
  },
  {
      "artist": "La Raíz",
      "name": "La Raíz ft. Rozalén - La hoguera de los continentes",
      "youtubeId": "RRkWBc3d7FQ"
  },
  {
      "artist": "Marea",
      "name": "Duerme Conmigo",
      "youtubeId": "SYALbYyrtE0"
  },
  {
      "artist": "Marea",
      "name": "La Rueca",
      "youtubeId": "HvjD7RxpxuU"
  },
  {
      "artist": "Ska-P",
      "name": "Intifada",
      "youtubeId": "4ReQFgWRAGg"
  },
  {
      "artist": "Mägo de Oz",
      "name": "Molinos de Viento",
      "youtubeId": "1gcfNE8txn4"
  },
  {
      "artist": "Héroes del Silencio",
      "name": "Entre dos tierras",
      "youtubeId": "SzimletXB7M"
  },
  {
      "artist": "Ska-P",
      "name": "Cannabis",
      "youtubeId": "cqQSeLDgBms"
  },
  {
      "artist": "Siniestro Total",
      "name": "Bailare sobre tu tumba",
      "youtubeId": "X8yLE3p9PgQ"
  },
  {
      "artist": "Ska-P",
      "name": "El Vals Del Obrero",
      "youtubeId": "65qjU0gEXX4"
  },
  {
      "artist": "Los Suaves",
      "name": "Dolores se llamaba Lola",
      "youtubeId": "R1Ep9V-SDXA"
  },
  {
      "artist": "Marea",
      "name": "Ciudad de los gitanos",
      "youtubeId": "QQqSNMHTXwk"
  },
  {
      "artist": "Boikot",
      "name": "De espaldas al mundo",
      "youtubeId": "gysmIxhW9zo"
  },
  {
      "artist": "Dover",
      "name": "Cherry Lee",
      "youtubeId": "-sZaiMnsE74"
  },
  {
      "artist": "La Raíz",
      "name": "Borracha y callejera",
      "youtubeId": "jEJNzlC84G4"
  },
  {
      "artist": "Marea",
      "name": "La luna me sabe a poco",
      "youtubeId": "NMfnLpdwuKA"
  },
  {
      "artist": "Ska-P",
      "name": "El Gato Lopez",
      "youtubeId": "OJNEx21hP1I"
  },
  {
      "artist": "Boikot",
      "name": "Sin tiempo para respirar",
      "youtubeId": "VggwLPqV6vs"
  },
  {
      "artist": "Marea",
      "name": "Romance de Jose Etxailarena",
      "youtubeId": "K421I8WYUjo"
  },
  {
      "artist": "Extremoduro",
      "name": "Golfa",
      "youtubeId": "oQynm5gQlQQ"
  },
  {
      "artist": "Siniestro Total",
      "name": "Ayatollah",
      "youtubeId": "bJrQlti_qT8"
  },
  {
      "artist": "Marea",
      "name": "Marea",
      "youtubeId": "KNlMz-wTdLg"
  },
  {
      "artist": "Marea",
      "name": "A la mierda primavera",
      "youtubeId": "AiuIjdhjGnw"
  },
  {
      "artist": "La Raíz",
      "name": "Nos Volveremos a Ver",
      "youtubeId": "CK_HCRUXnuo"
  },
  {
      "artist": "Ska-P",
      "name": "Welcome To Hell",
      "youtubeId": "od9cqr5BOgE"
  },
  {
      "artist": "Boikot",
      "name": "Tierra quemada",
      "youtubeId": "Ep3HLpOY1nY"
  },
  {
      "artist": "Extremoduro",
      "name": "Deltoya",
      "youtubeId": "ublbgaY7Fdg"
  },
  {
      "artist": "Ska-P",
      "name": "Juan sin tierra",
      "youtubeId": "bED_EoVtEQc"
  },
  {
      "artist": "Barón Rojo",
      "name": "Siempre estás allí",
      "youtubeId": "i732pTsrUIg"
  },
  {
      "artist": "La Raíz",
      "name": "Rueda la Corona",
      "youtubeId": "Q6yuB-CzbsM"
  },
  {
      "artist": "Mägo de Oz",
      "name": "Fiesta pagana",
      "youtubeId": "zZPvME8H2Oo"
  },
  {
      "artist": "Marea",
      "name": "El perro verde",
      "youtubeId": "x7bUHwzgedQ"
  },
  {
      "artist": "Ska-P",
      "name": "Romero El Madero",
      "youtubeId": "gL6ja95oRjM"
  },
  {
      "artist": "Medina Azahara",
      "name": "Necesito Respirar",
      "youtubeId": "hd3EMefTk7w"
  },
  {
      "artist": "Extremoduro",
      "name": "La vereda de la puerta de atrás",
      "youtubeId": "nIlIMnW5h5c"
  },
  {
      "artist": "Barón Rojo",
      "name": "Con Botas Sucias",
      "youtubeId": "EB5m_g5978M"
  },
  {
      "artist": "Boikot",
      "name": "Ska-lashnikov",
      "youtubeId": "EVyUlBQvdpU"
  },
  {
      "artist": "Boikot",
      "name": "Inés",
      "youtubeId": "Xbiav8Gd3MY"
  }
]