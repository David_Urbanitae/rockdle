export const rawSongs = {
    "kind": "youtube#searchListResponse",
    "etag": "nCNdsdClIfFBFrIcp1CVZQVbBdM",
    "nextPageToken": "CDIQAA",
    "regionCode": "ES",
    "pageInfo": {
      "totalResults": 1000000,
      "resultsPerPage": 50
    },
    "items": [
      {
        "kind": "youtube#searchResult",
        "etag": "1fYIiYpi6AWUIRfO5J9q_vmyHw4",
        "id": {
          "kind": "youtube#video",
          "videoId": "1snEYPg8TXs"
        },
        "snippet": {
          "publishedAt": "2020-02-21T16:00:01Z",
          "channelId": "UCjQhd1APsd5NQhiVZV7GYzg",
          "title": "SABATON - The Red Baron (Official Lyric Video)",
          "description": "The Official Lyric Video for The Red Baron by Sabaton from the album The Great War. ➞ SUBSCRIBE for more Sabaton: ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/1snEYPg8TXs/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/1snEYPg8TXs/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/1snEYPg8TXs/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Sabaton",
          "liveBroadcastContent": "none",
          "publishTime": "2020-02-21T16:00:01Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "7IZAYPFp58TxyUvZwhmKpWUAI40",
        "id": {
          "kind": "youtube#video",
          "videoId": "i732pTsrUIg"
        },
        "snippet": {
          "publishedAt": "2010-08-26T16:53:11Z",
          "channelId": "UCERa6QeThV5-J80hPiHBuYw",
          "title": "Barón rojo - Siempre estás allí (con letra)",
          "description": "Seguidme en Twitter @EstheerBradshaw La canción Siempre estás allí del grupo de rock español Barón rojo disco ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/i732pTsrUIg/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/i732pTsrUIg/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/i732pTsrUIg/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Lyricsen",
          "liveBroadcastContent": "none",
          "publishTime": "2010-08-26T16:53:11Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "GZI2aUW7NnLoyH5C2yr26Pg5nYo",
        "id": {
          "kind": "youtube#video",
          "videoId": "1QAwlwAv1iU"
        },
        "snippet": {
          "publishedAt": "2014-08-20T15:57:59Z",
          "channelId": "UCY0SsIPUI8qwvwcXhj6V5og",
          "title": "BARÓN ROJO - Ciro y Los Persas. Video Oficial",
          "description": "Seguí a CIRO y los Persas en Spotify acá: http://bit.ly/CiroylosPersasSpotify.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/1QAwlwAv1iU/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/1QAwlwAv1iU/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/1QAwlwAv1iU/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Ciro",
          "liveBroadcastContent": "none",
          "publishTime": "2014-08-20T15:57:59Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "RmiYEk1n9ATSYSSTejaDf8DYR8Y",
        "id": {
          "kind": "youtube#video",
          "videoId": "PTH3WS_zBGg"
        },
        "snippet": {
          "publishedAt": "2009-01-07T21:49:27Z",
          "channelId": "UCrsDpH8rCsgBlLE017Cr_Xg",
          "title": "Baron rojo - resistiré",
          "description": "Uno de los temas insignia del genial grupo Barón Rojo, Resistiré, el cual está en su disco Volumen Brutal.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/PTH3WS_zBGg/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/PTH3WS_zBGg/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/PTH3WS_zBGg/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "baronpeskadilla",
          "liveBroadcastContent": "none",
          "publishTime": "2009-01-07T21:49:27Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "EQaWU8gSabcy-A47kWl_Tls0NW0",
        "id": {
          "kind": "youtube#video",
          "videoId": "4BxTsqrlBZM"
        },
        "snippet": {
          "publishedAt": "2011-07-06T23:22:36Z",
          "channelId": "UCCMIvQ0OwKs8aSfe_wDH6Ow",
          "title": "Baron Rojo - Hijos De Cain",
          "description": "",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/4BxTsqrlBZM/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/4BxTsqrlBZM/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/4BxTsqrlBZM/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "UN MAN MUY LA VERGA",
          "liveBroadcastContent": "none",
          "publishTime": "2011-07-06T23:22:36Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "2UrNQ0E0Xjj-sMhos_ob3Ww8ykU",
        "id": {
          "kind": "youtube#video",
          "videoId": "9og-k5pBe4Y"
        },
        "snippet": {
          "publishedAt": "2014-02-14T01:39:08Z",
          "channelId": "UC2BVstOYzMCkxn5sR67XSOA",
          "title": "Baron Rojo Los Rockeros Van  Al Infierno Letra",
          "description": "",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/9og-k5pBe4Y/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/9og-k5pBe4Y/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/9og-k5pBe4Y/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Cristian Romero",
          "liveBroadcastContent": "none",
          "publishTime": "2014-02-14T01:39:08Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "b1KLLH5W_4eZLbXS1Nn9VvhhNqw",
        "id": {
          "kind": "youtube#video",
          "videoId": "iB9mO1tZgzA"
        },
        "snippet": {
          "publishedAt": "2014-11-09T05:26:18Z",
          "channelId": "UCWhMKNAiJVajRiGG7WdjXNQ",
          "title": "Hijos de Caín",
          "description": "Provided to YouTube by Zafiro Hijos de Caín · Baron Rojo En Un Lugar De La Marcha ℗ 1985 Serdisco Released on: 1985-07-01 ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/iB9mO1tZgzA/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/iB9mO1tZgzA/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/iB9mO1tZgzA/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Barón Rojo - Topic",
          "liveBroadcastContent": "none",
          "publishTime": "2014-11-09T05:26:18Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "rFy89ok0_SMMl2Nz1XnnB0tiZW4",
        "id": {
          "kind": "youtube#video",
          "videoId": "lZOch_xqyMM"
        },
        "snippet": {
          "publishedAt": "2014-11-08T06:26:10Z",
          "channelId": "UCWhMKNAiJVajRiGG7WdjXNQ",
          "title": "Siempre Estás Allí",
          "description": "Provided to YouTube by Zafiro Siempre Estás Allí · Baron Rojo Metalmorfosis ℗ 1983 Serdisco Released on: 1983-09-01 ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/lZOch_xqyMM/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/lZOch_xqyMM/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/lZOch_xqyMM/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Barón Rojo - Topic",
          "liveBroadcastContent": "none",
          "publishTime": "2014-11-08T06:26:10Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "1y1_7kERAl3K1gATx8tnImty6h0",
        "id": {
          "kind": "youtube#video",
          "videoId": "0XbluhblYNM"
        },
        "snippet": {
          "publishedAt": "2008-09-10T19:47:39Z",
          "channelId": "UCZWwLBodfbtnXQKQxcW45Qg",
          "title": "baron rojo-cuerdas de acero",
          "description": "Cancion cuerdas de acer, del grupo español baron rojo. Si os gusta visitad mis otros videos.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/0XbluhblYNM/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/0XbluhblYNM/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/0XbluhblYNM/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "puticlub1993",
          "liveBroadcastContent": "none",
          "publishTime": "2008-09-10T19:47:39Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "xq63M2xO5tJIbgVY_9PN3FtW-p0",
        "id": {
          "kind": "youtube#video",
          "videoId": "QXoFeK-82WY"
        },
        "snippet": {
          "publishedAt": "2006-01-16T20:07:26Z",
          "channelId": "UCBz6B1r56ln-C3IYKX5FREw",
          "title": "Hijos de Cain Baron Rojo",
          "description": "Cancion Hijos de Cain grupo Baron Rojo presentacion en TVE (Television Española) 1985.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/QXoFeK-82WY/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/QXoFeK-82WY/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/QXoFeK-82WY/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "cea sol",
          "liveBroadcastContent": "none",
          "publishTime": "2006-01-16T20:07:26Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "4OZKr-Y6dVaLYaOyuKXKNEVk6ps",
        "id": {
          "kind": "youtube#video",
          "videoId": "fWQpQYcQgP8"
        },
        "snippet": {
          "publishedAt": "2009-05-21T04:59:29Z",
          "channelId": "UCwUQHqX-1m3tEaKc3zV2-mw",
          "title": "Baron rojo- resistire",
          "description": "heavy metal en español.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/fWQpQYcQgP8/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/fWQpQYcQgP8/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/fWQpQYcQgP8/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Sigfrid89",
          "liveBroadcastContent": "none",
          "publishTime": "2009-05-21T04:59:29Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "Z0IFBvZuT528OgZGXR1ETLI-_Qc",
        "id": {
          "kind": "youtube#video",
          "videoId": "saM_nlFigNI"
        },
        "snippet": {
          "publishedAt": "2016-01-25T13:59:25Z",
          "channelId": "UCWtnb3ZoTdZijUGbiqVVGjw",
          "title": "El Baron Rojo - Der Rote Baron (2008) Audio Latino",
          "description": "Der Rote Baron es una película alemana del año 2008 dirigida por Nikolai Müllerschön. Trata sobre el conocido piloto de ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/saM_nlFigNI/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/saM_nlFigNI/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/saM_nlFigNI/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "oskar orlando malaguera garcia",
          "liveBroadcastContent": "none",
          "publishTime": "2016-01-25T13:59:25Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "icXCGtvywrhjy-xGMim5QmXA4CM",
        "id": {
          "kind": "youtube#video",
          "videoId": "Mu05uxmiQJo"
        },
        "snippet": {
          "publishedAt": "2009-05-23T07:20:10Z",
          "channelId": "UC6wgWsuVMB1EBLd4NOKrHrg",
          "title": "Baron Rojo - Barón Rojo (Video no oficial)",
          "description": "Videoclip realizado con imágenes obtenidas del Barón Rojo en internet sobre fondo musical de la canción del mismo grupo.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/Mu05uxmiQJo/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/Mu05uxmiQJo/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/Mu05uxmiQJo/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "hefestos68",
          "liveBroadcastContent": "none",
          "publishTime": "2009-05-23T07:20:10Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "0SbNftA_S-RLWfpxLfjILbioVYo",
        "id": {
          "kind": "youtube#video",
          "videoId": "4E3--jNfmnQ"
        },
        "snippet": {
          "publishedAt": "2018-07-19T11:33:25Z",
          "channelId": "UCWhMKNAiJVajRiGG7WdjXNQ",
          "title": "Los Rockeros Van al Infierno",
          "description": "Provided to YouTube by Sony Music Entertainment Los Rockeros Van al Infierno · Baron Rojo Grandes Exitos ℗ 1981 Serdisco ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/4E3--jNfmnQ/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/4E3--jNfmnQ/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/4E3--jNfmnQ/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Barón Rojo - Topic",
          "liveBroadcastContent": "none",
          "publishTime": "2018-07-19T11:33:25Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "EjjwvxokvjgPDVeKDnoYnmZCZow",
        "id": {
          "kind": "youtube#video",
          "videoId": "dQPiE0qN_gE"
        },
        "snippet": {
          "publishedAt": "2014-02-23T23:25:48Z",
          "channelId": "UCat3jyiArV7ncy6O58wHkTQ",
          "title": "El Baron Rojo Opening Full Latino (beta)",
          "description": "Es una versión beta porque no me convence, espero poder mejorarla XD MP3 Actualizado (me sigue sin convencer) xd ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/dQPiE0qN_gE/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/dQPiE0qN_gE/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/dQPiE0qN_gE/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Crisz Reyes",
          "liveBroadcastContent": "none",
          "publishTime": "2014-02-23T23:25:48Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "xtwaaOQu-FDnCcG8Nxuq89-N6j4",
        "id": {
          "kind": "youtube#video",
          "videoId": "L3d7uNj3QCk"
        },
        "snippet": {
          "publishedAt": "2009-05-15T11:45:29Z",
          "channelId": "UCbElDCgxESyRMYZncNRqJYA",
          "title": "BARÓN ROJO - &quot; El Malo &quot;",
          "description": "Actuación de Baron Rojo en el programa de Tve Tocata.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/L3d7uNj3QCk/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/L3d7uNj3QCk/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/L3d7uNj3QCk/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Isma1972",
          "liveBroadcastContent": "none",
          "publishTime": "2009-05-15T11:45:29Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "02ZxPGZ1Rqe7U1fGu2k2-8VTHP0",
        "id": {
          "kind": "youtube#video",
          "videoId": "VHImt_iU0YA"
        },
        "snippet": {
          "publishedAt": "2018-07-19T11:33:30Z",
          "channelId": "UCWhMKNAiJVajRiGG7WdjXNQ",
          "title": "Resistiré",
          "description": "Provided to YouTube by Sony Music Entertainment Resistiré · Baron Rojo Grandes Exitos ℗ 1981 Serdisco Released on: ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/VHImt_iU0YA/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/VHImt_iU0YA/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/VHImt_iU0YA/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Barón Rojo - Topic",
          "liveBroadcastContent": "none",
          "publishTime": "2018-07-19T11:33:30Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "2jLr-f4Y3G2tjAdSvUl1NxfcGT4",
        "id": {
          "kind": "youtube#video",
          "videoId": "3qAd8385PXA"
        },
        "snippet": {
          "publishedAt": "2009-02-07T21:54:47Z",
          "channelId": "UCB8dNSum4Fg1tMTpI76_7FA",
          "title": "Barón Rojo Larga vida al Rock al Roll",
          "description": "clasico de españa...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/3qAd8385PXA/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/3qAd8385PXA/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/3qAd8385PXA/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "sexonarth",
          "liveBroadcastContent": "none",
          "publishTime": "2009-02-07T21:54:47Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "4Dgdq-5tW2QBPrfXWCSZSbGk8vI",
        "id": {
          "kind": "youtube#video",
          "videoId": "F36eaLW6yLA"
        },
        "snippet": {
          "publishedAt": "2006-01-15T21:17:06Z",
          "channelId": "UCBz6B1r56ln-C3IYKX5FREw",
          "title": "Baron Rojo",
          "description": "Cancion Baron Rojo del grupo Baron Rojo abriendo el concierto del Baron al Rojo Vivo en Madrid 1984.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/F36eaLW6yLA/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/F36eaLW6yLA/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/F36eaLW6yLA/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "cea sol",
          "liveBroadcastContent": "none",
          "publishTime": "2006-01-15T21:17:06Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "5tNl0RdxqBEZOhWr8_R1tUsPfa8",
        "id": {
          "kind": "youtube#video",
          "videoId": "UylnUxKShvE"
        },
        "snippet": {
          "publishedAt": "2015-06-16T00:45:59Z",
          "channelId": "UCIdb8Su6Ti4NXQlRhT9pVlg",
          "title": "BARON ROJO -siempre estas allí 30 aniversario FORMACIÓN ORIGINAL",
          "description": "UNA DE LAS MEJORES BALADAS DEL ROCK EN ESPAÑOL SIN DUDA ALGUNA,,TREMENDO CLÁSICO.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/UylnUxKShvE/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/UylnUxKShvE/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/UylnUxKShvE/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "jose alfredo retro show",
          "liveBroadcastContent": "none",
          "publishTime": "2015-06-16T00:45:59Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "13hqppNooiMBC5v_fftILeC-D_Q",
        "id": {
          "kind": "youtube#video",
          "videoId": "K-l7IHLSVZo"
        },
        "snippet": {
          "publishedAt": "2018-02-05T21:48:53Z",
          "channelId": "UC6epAxOHZ8WjTmjPmHQo-kQ",
          "title": "Metallica - Los rockeros van al infierno (Barón Rojo) - Madrid 2018",
          "description": "Metallica hace una versión de Barón Rojo, \"Los rockeros van al infierno\", en el concierto de Madrid (WiZink Center - Palacio de ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/K-l7IHLSVZo/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/K-l7IHLSVZo/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/K-l7IHLSVZo/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "MariskalRockTV",
          "liveBroadcastContent": "none",
          "publishTime": "2018-02-05T21:48:53Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "P-Iqj2ZylYefRTQXe2y9YhOrs-4",
        "id": {
          "kind": "youtube#video",
          "videoId": "Pst1MIWVw1A"
        },
        "snippet": {
          "publishedAt": "2008-11-27T21:19:20Z",
          "channelId": "UCyLHa_CUJUq5HorSdYuNWAA",
          "title": "Barón rojo - 07 El malo",
          "description": "Barón rojo - Metalmorfosis http://www.baronrojo.net/",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/Pst1MIWVw1A/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/Pst1MIWVw1A/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/Pst1MIWVw1A/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Trenck",
          "liveBroadcastContent": "none",
          "publishTime": "2008-11-27T21:19:20Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "QtKsHJ65uAk8y9TyT7zLwJapD4o",
        "id": {
          "kind": "youtube#video",
          "videoId": "g6HolknMwIQ"
        },
        "snippet": {
          "publishedAt": "2011-05-01T22:29:24Z",
          "channelId": "UCA-9Spii632hALGVWHhxtPA",
          "title": "BARON ROJO - RESISTIRE (VIDEO CLIP)",
          "description": "UN TEMAZO que a pesar de tener ya 30 años,sigue teniendo vida propia este pedazo de letra (RESISTIRE) LARGA VIDA AL ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/g6HolknMwIQ/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/g6HolknMwIQ/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/g6HolknMwIQ/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "archiluz",
          "liveBroadcastContent": "none",
          "publishTime": "2011-05-01T22:29:24Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "Rdnf1Q0pplwljsHe0jw0CiwXcBg",
        "id": {
          "kind": "youtube#video",
          "videoId": "LZg4hSP1lCM"
        },
        "snippet": {
          "publishedAt": "2009-03-26T12:48:47Z",
          "channelId": "UCyLHa_CUJUq5HorSdYuNWAA",
          "title": "Barón Rojo 04 Tierra De Nadie",
          "description": "Barón rojo - Tierra de Nadie http://www.baronrojo.net/",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/LZg4hSP1lCM/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/LZg4hSP1lCM/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/LZg4hSP1lCM/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Trenck",
          "liveBroadcastContent": "none",
          "publishTime": "2009-03-26T12:48:47Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "DTlL0_sp3heiYgUfzHdtehqz8Qs",
        "id": {
          "kind": "youtube#video",
          "videoId": "RoSkY1-Unj0"
        },
        "snippet": {
          "publishedAt": "2022-07-15T21:18:36Z",
          "channelId": "UC-f2WBfSCZiu0bOBydjot3w",
          "title": "The Red Baron - Remastered",
          "description": "Der Rote Kampfflieger, The King of the Sky, Ace of Aces. However you know him, one fighter pilot's name stands out in history.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/RoSkY1-Unj0/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/RoSkY1-Unj0/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/RoSkY1-Unj0/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Yarnhub",
          "liveBroadcastContent": "none",
          "publishTime": "2022-07-15T21:18:36Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "Ut491VHdAimhYgKbwfZQb2LajDU",
        "id": {
          "kind": "youtube#video",
          "videoId": "lMxjJ1I3uTg"
        },
        "snippet": {
          "publishedAt": "2008-04-24T23:57:51Z",
          "channelId": "UCqtn6nzN3MJ3DGNhI_h6DtA",
          "title": "BARON ROJO - Tierra de Nadie",
          "description": "Baron Rojo - Un pequeño Tributo como Aporte a unas de mis Bandas Prefe entre mis Peferidas.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/lMxjJ1I3uTg/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/lMxjJ1I3uTg/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/lMxjJ1I3uTg/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Daniel Rodriguez",
          "liveBroadcastContent": "none",
          "publishTime": "2008-04-24T23:57:51Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "EFX11K221sgPqB5qIJ-lspsJm_Y",
        "id": {
          "kind": "youtube#video",
          "videoId": "PWq5NozNvWw"
        },
        "snippet": {
          "publishedAt": "2008-10-19T14:53:02Z",
          "channelId": "UCyLHa_CUJUq5HorSdYuNWAA",
          "title": "Barón rojo - 04 - Son como hormigas",
          "description": "Barón rojo - Volumen brutal 1982 http://www.baronrojo.net/",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/PWq5NozNvWw/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/PWq5NozNvWw/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/PWq5NozNvWw/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Trenck",
          "liveBroadcastContent": "none",
          "publishTime": "2008-10-19T14:53:02Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "uU0GsLLaOBvY3qvmq2dfuWvBrLY",
        "id": {
          "kind": "youtube#video",
          "videoId": "EB5m_g5978M"
        },
        "snippet": {
          "publishedAt": "2013-07-05T05:06:57Z",
          "channelId": "UC1pz2Ght2EQ-HfjVZEUBJoQ",
          "title": "Baron Rojo - Con Botas Sucias",
          "description": "",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/EB5m_g5978M/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/EB5m_g5978M/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/EB5m_g5978M/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Legnatas",
          "liveBroadcastContent": "none",
          "publishTime": "2013-07-05T05:06:57Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "LgOHJGnCm0mYLHIvD2FKFs5GCro",
        "id": {
          "kind": "youtube#video",
          "videoId": "zmbjS-_FAQs"
        },
        "snippet": {
          "publishedAt": "2008-10-02T16:38:42Z",
          "channelId": "UCbjTjllR4exX4S6Muy3-h3A",
          "title": "Baron Rojo - Chicos del Rock",
          "description": "Track taken from \"En Un Lugar de la Marcha\" 1985.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/zmbjS-_FAQs/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/zmbjS-_FAQs/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/zmbjS-_FAQs/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Kristoss666",
          "liveBroadcastContent": "none",
          "publishTime": "2008-10-02T16:38:42Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "rC3RMFE01Z7-RYvJLK69ikglZ4o",
        "id": {
          "kind": "youtube#video",
          "videoId": "poHyTr5Rck0"
        },
        "snippet": {
          "publishedAt": "2006-01-06T05:16:28Z",
          "channelId": "UCBz6B1r56ln-C3IYKX5FREw",
          "title": "Baron Rojo Concierto para Ellos",
          "description": "Video del grupo Español Baron Rojo de la cancion Concierto para Ellos con tomas del concierto de el sábado 10 de Noviembre ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/poHyTr5Rck0/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/poHyTr5Rck0/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/poHyTr5Rck0/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "cea sol",
          "liveBroadcastContent": "none",
          "publishTime": "2006-01-06T05:16:28Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "fCuo3oaxzP0m_d-i9C9E546fyGg",
        "id": {
          "kind": "youtube#video",
          "videoId": "g1vH8hGtyHo"
        },
        "snippet": {
          "publishedAt": "2007-09-26T13:48:53Z",
          "channelId": "UC3kuEiIihZy7zq4EYNqCzVg",
          "title": "Baron Rojo - Casi me mato",
          "description": "Baron Rojo - Casi me mato ...¿digo algo sobre esta cancion?..creo k no...pero si sobre coger bichitos con martillos?",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/g1vH8hGtyHo/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/g1vH8hGtyHo/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/g1vH8hGtyHo/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "salserin4h",
          "liveBroadcastContent": "none",
          "publishTime": "2007-09-26T13:48:53Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "RrNewVhW7N1NFHBGPa7zu8R18N0",
        "id": {
          "kind": "youtube#video",
          "videoId": "AuF0gTl2mto"
        },
        "snippet": {
          "publishedAt": "2021-10-01T22:31:40Z",
          "channelId": "UCZowf26DsJ0mL1Arj60A51w",
          "title": "Ángel Valera canta &#39;Barón Rojo&#39; | Audiciones a ciegas | La Voz Antena 3 2021",
          "description": "Suscríbete al canal de La Voz: http://atres.red/tbj7e La Voz en ATRESplayer: ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/AuF0gTl2mto/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/AuF0gTl2mto/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/AuF0gTl2mto/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "La Voz Antena 3",
          "liveBroadcastContent": "none",
          "publishTime": "2021-10-01T22:31:40Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "TVH6FhpcDi1EULgl1Yov22pJZpI",
        "id": {
          "kind": "youtube#video",
          "videoId": "NY56z6b3Y4c"
        },
        "snippet": {
          "publishedAt": "2019-09-24T23:08:04Z",
          "channelId": "UCbMY5LInV0Z_qry_6kgXqdA",
          "title": "Docu-Resumen: EL BARÓN ROJO ¿Mucho más que robots y peleas?",
          "description": "Resumen / Curiosidades de El Barón Rojo, un anime de género Mecha emitido en 1994. La historia gira en torno al joven piloto ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/NY56z6b3Y4c/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/NY56z6b3Y4c/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/NY56z6b3Y4c/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Drey Dareptil",
          "liveBroadcastContent": "none",
          "publishTime": "2019-09-24T23:08:04Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "p-La7eFwWoVUV_U41I3CtmZ4D3c",
        "id": {
          "kind": "youtube#video",
          "videoId": "QxhFvkJlK4Q"
        },
        "snippet": {
          "publishedAt": "2012-09-26T05:34:05Z",
          "channelId": "UCypi8_0WzNmEwp_Va29429Q",
          "title": "El Baron Rojo Capitulo 44 - El Otro Ken (COMPLETO)",
          "description": "Capitulo 44: El Otro Ken AUDIO LATINO ¡ SUSCRIBETE !",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/QxhFvkJlK4Q/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/QxhFvkJlK4Q/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/QxhFvkJlK4Q/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "frankielavoz",
          "liveBroadcastContent": "none",
          "publishTime": "2012-09-26T05:34:05Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "ULbM6kMqLFtFZuxjaAF7fF6i134",
        "id": {
          "kind": "youtube#video",
          "videoId": "CYOpSG6Siqw"
        },
        "snippet": {
          "publishedAt": "2008-06-15T22:57:59Z",
          "channelId": "UCvvAjBc9nX0SxQoavLtavdQ",
          "title": "Baron Rojo - Los Rockeros van al Infierno",
          "description": "Se oyen comentar a las gentes del lugra los rokeros no son buenos....MI ROLLO ES EL ROCK!gran grupo español concierto del ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/CYOpSG6Siqw/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/CYOpSG6Siqw/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/CYOpSG6Siqw/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "JuankiMetall",
          "liveBroadcastContent": "none",
          "publishTime": "2008-06-15T22:57:59Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "TKiPR7oNJr3Fp4ubeYh0iHCZAS0",
        "id": {
          "kind": "youtube#video",
          "videoId": "Un5OkLk1Er0"
        },
        "snippet": {
          "publishedAt": "2011-08-20T01:22:13Z",
          "channelId": "UCrP5mIIYQtEnJLHwy8DsYnA",
          "title": "BARON ROJO - &quot;CAMPO DE CONCENTRACION&quot; (Baron Al Rojo Vivo)",
          "description": "Video con el Audio mejorado. Tributo al Barón desde Cali - Colombia (Suramérica). Larga Vida Al Barón !!!",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/Un5OkLk1Er0/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/Un5OkLk1Er0/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/Un5OkLk1Er0/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Cavernet Rock",
          "liveBroadcastContent": "none",
          "publishTime": "2011-08-20T01:22:13Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "aqrVvSwpb5un-86pa8q2QI-zg8U",
        "id": {
          "kind": "youtube#video",
          "videoId": "wwB779BDMY4"
        },
        "snippet": {
          "publishedAt": "2016-02-20T09:51:43Z",
          "channelId": "UCrhanzJX5FQeAU8kUf2F3eA",
          "title": "Baron Rojo - Baron al Rojo Vivo (Videoclip Oficial)",
          "description": "Canción: Barón Rojo Autores: José Luis Campuzano (Sherpa) y Carolina Cortés Perteneciente al disco \"Barón al Rojo Vivo\" (c) ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/wwB779BDMY4/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/wwB779BDMY4/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/wwB779BDMY4/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "LAVOZ DEMIYO",
          "liveBroadcastContent": "none",
          "publishTime": "2016-02-20T09:51:43Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "5S_H-UdqEI8lWtiWu_1Wwv7r0FI",
        "id": {
          "kind": "youtube#video",
          "videoId": "UMIZhq_admw"
        },
        "snippet": {
          "publishedAt": "2010-09-21T00:46:02Z",
          "channelId": "UCxdBy7jd8wlmV2_paKb3ZlA",
          "title": "BARON ROJO",
          "description": "Colgado por luis rl dibujo animado más visto.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/UMIZhq_admw/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/UMIZhq_admw/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/UMIZhq_admw/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Luis Angel Masfil",
          "liveBroadcastContent": "none",
          "publishTime": "2010-09-21T00:46:02Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "9CUR4o1NqMD4tkaWhGT_NpAui8I",
        "id": {
          "kind": "youtube#video",
          "videoId": "N6njxpq8QDo"
        },
        "snippet": {
          "publishedAt": "2010-08-28T14:32:27Z",
          "channelId": "UCERa6QeThV5-J80hPiHBuYw",
          "title": "Baron rojo - Baron rojo (con letra)",
          "description": "Canción Baron rojo del grupo de rock español Baron rojo (con letra).",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/N6njxpq8QDo/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/N6njxpq8QDo/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/N6njxpq8QDo/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Lyricsen",
          "liveBroadcastContent": "none",
          "publishTime": "2010-08-28T14:32:27Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "xg4jknXI2cExda2yZcDksAbYgLY",
        "id": {
          "kind": "youtube#video",
          "videoId": "lHh1SKhWS0g"
        },
        "snippet": {
          "publishedAt": "2022-03-23T19:24:09Z",
          "channelId": "UCirAgCSd8UhplbWvP8t0d0Q",
          "title": "el fantasma de kiev y el barón rojo 🤔",
          "description": "",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/lHh1SKhWS0g/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/lHh1SKhWS0g/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/lHh1SKhWS0g/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Curiosidad⚡ST💡",
          "liveBroadcastContent": "none",
          "publishTime": "2022-03-23T19:24:09Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "toDaNtpTRCPmmTOkExvaBD76Zvg",
        "id": {
          "kind": "youtube#video",
          "videoId": "2JNua8aBl1I"
        },
        "snippet": {
          "publishedAt": "2015-04-16T19:19:53Z",
          "channelId": "UC0_8LB9siUpY84Q1VqI-Rpw",
          "title": "AMERICA PARA EL PUEBLO - LA VOZ DE LA CALLE ♪ - COSTELLO  //  BARON ROJO SUR",
          "description": "BARON ROJO SUR - PALMIRA LA 21 Y LAS FILIALES PRESENTES. #AMERICAPARAELPUEBLO INSTAGRAM ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/2JNua8aBl1I/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/2JNua8aBl1I/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/2JNua8aBl1I/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "fernandoriesgo",
          "liveBroadcastContent": "none",
          "publishTime": "2015-04-16T19:19:53Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "ZACCBXtaLs6NTmK1TRoPA9V7PiI",
        "id": {
          "kind": "youtube#video",
          "videoId": "lTqneCYfxJ0"
        },
        "snippet": {
          "publishedAt": "2010-02-07T16:05:05Z",
          "channelId": "UCXNj1LI2yCFp9bro9FfuXAg",
          "title": "BARON ROJO-LOS ROCKEROS SE VAN AL INFIERNO 2007",
          "description": "Los rockeros se van al infierno, Desde Baron a Bilbao. Aste Nagusia. 23 de Agosto 2007.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/lTqneCYfxJ0/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/lTqneCYfxJ0/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/lTqneCYfxJ0/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Victor Ross",
          "liveBroadcastContent": "none",
          "publishTime": "2010-02-07T16:05:05Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "yA1u0CYBKIWBCKmW6z5zATUZJiM",
        "id": {
          "kind": "youtube#video",
          "videoId": "c_uS2d2T_KU"
        },
        "snippet": {
          "publishedAt": "2008-11-27T21:37:49Z",
          "channelId": "UCyLHa_CUJUq5HorSdYuNWAA",
          "title": "Barón rojo - 06 Hiroshima",
          "description": "Barón rojo - Metalmorfosis http://www.baronrojo.net/",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/c_uS2d2T_KU/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/c_uS2d2T_KU/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/c_uS2d2T_KU/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Trenck",
          "liveBroadcastContent": "none",
          "publishTime": "2008-11-27T21:37:49Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "4E5dUWIywob8aC69obdL-G-U5sE",
        "id": {
          "kind": "youtube#video",
          "videoId": "vga_IDSjAeg"
        },
        "snippet": {
          "publishedAt": "2018-09-04T04:09:32Z",
          "channelId": "UCG9ZB-AVgRU5EUztMNFov8A",
          "title": "El Baron Rojo Opening 2 latino",
          "description": "",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/vga_IDSjAeg/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/vga_IDSjAeg/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/vga_IDSjAeg/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "JRPRO",
          "liveBroadcastContent": "none",
          "publishTime": "2018-09-04T04:09:32Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "Q1-TGHBHD0u8kEE9gLlzcCry8Ts",
        "id": {
          "kind": "youtube#video",
          "videoId": "0aYekYdbgd4"
        },
        "snippet": {
          "publishedAt": "2006-01-25T23:51:25Z",
          "channelId": "UCBz6B1r56ln-C3IYKX5FREw",
          "title": "Breakthoven Baron Rojo",
          "description": "Cancion Breakthoven Baron Rojo en vivo.",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/0aYekYdbgd4/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/0aYekYdbgd4/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/0aYekYdbgd4/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "cea sol",
          "liveBroadcastContent": "none",
          "publishTime": "2006-01-25T23:51:25Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "lzBEBhhkQNtOUo80_P47aZVzOEQ",
        "id": {
          "kind": "youtube#video",
          "videoId": "uq_FQI7j81Q"
        },
        "snippet": {
          "publishedAt": "2014-02-16T01:19:36Z",
          "channelId": "UCzHo19UQ4HsEH5waOx707YA",
          "title": "Barón Rojo en Buenos Aires (Argentina), 05-10-1983.",
          "description": "Concierto de Barón Rojo en Buenos Aires (Argentina), celebrado el día 5 de octubre de 1983 en el Estadio Obras Sanitarias de la ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/uq_FQI7j81Q/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/uq_FQI7j81Q/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/uq_FQI7j81Q/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Barón Rojo",
          "liveBroadcastContent": "none",
          "publishTime": "2014-02-16T01:19:36Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "edK_TuaU_DRXZnj1Aur1dVcsynw",
        "id": {
          "kind": "youtube#video",
          "videoId": "GzpeQoTOaVY"
        },
        "snippet": {
          "publishedAt": "2011-01-10T18:53:35Z",
          "channelId": "UCA-9Spii632hALGVWHhxtPA",
          "title": "BARON ROJO -Tierra de nadie",
          "description": "Un lujo de letra DE BARON ROJO en un mundo cada dia mas loco y feroz, donde ya nada es lo mismo y todo vale ,hay algunas ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/GzpeQoTOaVY/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/GzpeQoTOaVY/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/GzpeQoTOaVY/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "archiluz",
          "liveBroadcastContent": "none",
          "publishTime": "2011-01-10T18:53:35Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "rp8RHPbtwrgdoc1BrTwdhq45HQg",
        "id": {
          "kind": "youtube#video",
          "videoId": "CORLlURGBC0"
        },
        "snippet": {
          "publishedAt": "2012-09-27T22:20:53Z",
          "channelId": "UCypi8_0WzNmEwp_Va29429Q",
          "title": "El Baron Rojo Capitulo 47 - El batallón solitario destruye al barón de la muerte (COMPLETO)",
          "description": "Capitulo 47: El batallón solitario destruye al barón de la muerte AUDIO LATINO ¡ SUSCRIBETE !",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/CORLlURGBC0/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/CORLlURGBC0/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/CORLlURGBC0/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "frankielavoz",
          "liveBroadcastContent": "none",
          "publishTime": "2012-09-27T22:20:53Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "j47MX4NudQJwcBSTuLlQCo0jGg0",
        "id": {
          "kind": "youtube#video",
          "videoId": "12lr1qnbj_8"
        },
        "snippet": {
          "publishedAt": "2020-04-09T16:45:00Z",
          "channelId": "UCNupUKvCYkILVvwSrPTrllA",
          "title": "El barón rojo (1971) | HD español - castellano",
          "description": "Reconstrucción de las hazañas bélicas del célebre As de la aviación alemán de la Primera Guerra Mundial, el Barón Manfred ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/12lr1qnbj_8/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/12lr1qnbj_8/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/12lr1qnbj_8/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Warlord",
          "liveBroadcastContent": "none",
          "publishTime": "2020-04-09T16:45:00Z"
        }
      },
      {
        "kind": "youtube#searchResult",
        "etag": "Lp4MRyfZOlb9f8B-kuQUQTqAD98",
        "id": {
          "kind": "youtube#video",
          "videoId": "zPJe5NjbNzE"
        },
        "snippet": {
          "publishedAt": "2008-12-21T21:56:26Z",
          "channelId": "UCYD7QC9Pj5d2v5XIgkJr6_Q",
          "title": "Baron Rojo-Tren Fantasma",
          "description": "De lo mejor del heavy metal en español,con Baron Rojo y un classico como lo es tren fantasma. aqui les dejo la biografia de esta ...",
          "thumbnails": {
            "default": {
              "url": "https://i.ytimg.com/vi/zPJe5NjbNzE/default.jpg",
              "width": 120,
              "height": 90
            },
            "medium": {
              "url": "https://i.ytimg.com/vi/zPJe5NjbNzE/mqdefault.jpg",
              "width": 320,
              "height": 180
            },
            "high": {
              "url": "https://i.ytimg.com/vi/zPJe5NjbNzE/hqdefault.jpg",
              "width": 480,
              "height": 360
            }
          },
          "channelTitle": "Ever Johel Lemus",
          "liveBroadcastContent": "none",
          "publishTime": "2008-12-21T21:56:26Z"
        }
      }
    ]
  }
  