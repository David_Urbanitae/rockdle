import { GuessType } from "../types/guess";

export function scoreToEmoji(guesses: GuessType[], text: string): string {
  const emojis = {
    incorrect: "🟥",
    correct: "🟩",
    skip: "⬜",
    empty: "⬛️",
  };
  const todaysDate = new Date();
  const prefix = `Rockdle - ${todaysDate.toLocaleDateString()} 🎧`;

  let scoreEmoji = "";

  guesses.forEach((guess: GuessType) => {
    if (guess.isCorrect === true) {
      scoreEmoji += emojis.correct;
    } else if (guess.skipped === true) {
      scoreEmoji += emojis.skip;
    } else if (guess.isCorrect === false) {
      scoreEmoji += emojis.incorrect;
    } else {
      scoreEmoji += emojis.empty;
    }
  });

  return `${prefix}\n${text}\nhttps://rockdle.vercel.app\n${scoreEmoji}`;
}
