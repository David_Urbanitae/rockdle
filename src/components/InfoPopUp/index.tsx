import React from "react";
import { IoMusicalNoteOutline, IoHelpCircleOutline } from "react-icons/io5";
import { Button } from "..";

import * as Styled from "./index.styled";

interface Props {
  onClose: () => void;
}

export function InfoPopUp({ onClose }: Props) {
  return (
    <Styled.Container>
      <Styled.PopUp>
        <h1>¡Mola más que el otro! 👋</h1>
        <Styled.Spacer />
        <Button variant="green" style={{ marginTop: 20 }} onClick={onClose}>
          Cerrar
        </Button>
      </Styled.PopUp>
    </Styled.Container>
  );
}
