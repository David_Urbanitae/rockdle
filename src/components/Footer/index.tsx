import React from "react";
import { IoHeart } from "react-icons/io5";

import * as Styled from "./index.styled";

export function Footer() {
  return (
    <footer>
      <Styled.Text>
        Plagiarized with <IoHeart /> by{" "}
        <Styled.Link href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
          David G. Ortiz
        </Styled.Link>
      </Styled.Text>
    </footer>
  );
}
