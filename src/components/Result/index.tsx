import React from "react";

import { Song } from "../../types/song";
import { GuessType } from "../../types/guess";
import { scoreToEmoji } from "../../helpers";

import { Button } from "../Button";
import { YouTube } from "../YouTube";

import * as Styled from "./index.styled";

interface Props {
  didGuess: boolean;
  currentTry: number;
  todaysSolution: Song;
  guesses: GuessType[];
}

export function Result({
  didGuess,
  todaysSolution,
  guesses,
  currentTry,
}: Props) {
  const hoursToNextDay = Math.floor(
    (new Date(new Date().setHours(24, 0, 0, 0)).getTime() -
      new Date().getTime()) /
      1000 /
      60 /
      60
  );

  const textForTry = ["¡Flamísima!", "De locos", "No tah mal", "Messirve", "Pena máxima"];

  if (didGuess) {
    const copyResult = React.useCallback(() => {
      navigator.clipboard.writeText(scoreToEmoji(guesses, textForTry[currentTry - 1]));
    }, [guesses]);

    const triesConjugation = currentTry === 1 ? "intento" : "intentos";

    return (
      <>
        <Styled.ResultTitle>{textForTry[currentTry - 1]}</Styled.ResultTitle>
        <Styled.SongTitle>
          La canción de hoy es {todaysSolution.artist} -{" "}
          {todaysSolution.name}
        </Styled.SongTitle>
        <Styled.Tries>
          Lograste adivinar en {currentTry} {triesConjugation}
        </Styled.Tries>
        <YouTube id={todaysSolution.youtubeId} />
        <Button onClick={copyResult} variant="green">
          Copiar resultado
        </Button>
        <Styled.TimeToNext>
          Recuerda volver mañana - el próximo Rockdle en: {hoursToNextDay}{" "}
          horas
        </Styled.TimeToNext>
      </>
    );
  } else {
    return (
      <>
        <Styled.ResultTitle>Vaya, cagaste...</Styled.ResultTitle>
        <Styled.SongTitle>
        La canción de hoy es {todaysSolution.artist} -{" "}
          {todaysSolution.name}
        </Styled.SongTitle>
        <YouTube id={todaysSolution.youtubeId} />
        <Styled.TimeToNext>
        Recuerda volver mañana - el próximo Rockdle en: {hoursToNextDay}{" "}
          horas
        </Styled.TimeToNext>
      </>
    );
  }
}
